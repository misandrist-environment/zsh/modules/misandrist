#
# Misandrist customizations
#
# Authors:
#   Evan Cofsky <evan@theunixman.com>
#

_init_misandrist() {
    _dircolors () {
        zstyle -b ':prezto:module:misandrist:dircolors' internal
        zstyle -b ':prezto:module:misandrist:dircolors' install

        local -r ma_dircolors="$HOME/.zprezto/modules/misandrist/dircolors"
        local -r home_dircolors="$HOME/.dircolors"
        if [[ "$internal" = "yes" ]]; then
            readonly dcrcpath="${ma_dircolors}"
        else
            readonly dcrdpath="${home_dircolors}"
            if [[ "$install" = "yes" && ! -r "${dcrdpath}" ]]; then
                (cd $HOME && ln -s "${ma_dircolors} .dircolors")
            else
                echo "WARN: No file ${dcrdpath} but neither internal nor install specified. " >&2
            fi
        fi

        [ -f "${dcrdpath}" ] && eval $(dircolors "${dcrdpath}")
    }

    function _set_extra_paths () {
        local depth=4
        local -aU _extra_root_paths

        zstyle -a ':prezto:module:misandrist:paths' roots _extra_root_paths


        _find_extra_paths() {
            local -aU f=($(find "${_extra_root_paths[@]}" \
                                -mindepth 1 \
                                -maxdepth $depth \
                                -type d \
                                "$@")
                        )
            echo "${f[@]}"
        }

        local -aU bins=($(_find_extra_paths -name 'bin'))
        path=(${bins[@]} $path[@])

        local -aU cmakes=(
            $(_find_extra_paths \
                  ! -regex '.*/data/.*' \
                  -iname 'cmake*'
            )
        )
        local -aU pkg_cfgs=($(_find_extra_paths -iname '*pkg*conf*'))

        export CMAKE_MODULE_PATH=${(j.:.)cmakes:+:${CMAKE_MODULE_PATH}}

        export PKG_CONFIG_PATH=${(j.:.)pkg_cfgs:+:${PKG_CONFIG_PATH}}
    }

    path=(/home/evan/.emacs.d/term-cmd $path)

    _set_extra_paths

    export OPENCL_ROOT=/opt/rocm/opencl
    export AMDAPPSDK=/opt/rocm/opencl

    export CC=$(which clang)
    export CXX=$(which clang++)

    eval "$(direnv hook zsh)"
}

_init_misandrist

unset _init_misandrist
